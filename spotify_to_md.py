import spotipy
import requests
from PIL import Image
from mdutils.mdutils import MdUtils
from spotipy.oauth2 import SpotifyOAuth
import re, string; 

pattern = re.compile('[\W_]+')
scope = "user-library-read"

sp = spotipy.Spotify(auth_manager=SpotifyOAuth(scope=scope))

offset = 0
total_albums = 0
while (offset <= total_albums):
    results = sp.current_user_saved_albums(50, offset)
    for idx, item in enumerate(results['items']):
        album = item['album']
        print(idx, album['artists'][0]['name'], " – ", album['name'], album['release_date'])

        filename = re.sub(pattern,' ',album['artists'][0]['name'] + " " + album['name']).replace(" ", "_").lower()
        mdFile = MdUtils(file_name="albums/"+filename, title=album['artists'][0]['name'] + " - " + album['name'])
        mdFile.new_line(mdFile.new_reference_image(text=album['artists'][0]['name'] + " " + album['name'], path='../../covers/dithering/'+filename+'.png', reference_tag='im'))

        mdFile.create_md_file()

        r = requests.get(album['images'][0]['url'])
        open('covers/originals/'+filename+'.png', 'wb').write(r.content)
        
        img = Image.open('covers/originals/'+filename+'.png')
        width, height = img.size
        new_width = 500
        new_height = int(height * new_width / width)
        img = img.resize((new_width, new_height), Image.LANCZOS)
        img = img.convert('P')
        img.save('covers/dithering/'+filename+'.png', format="png", optimize=True, quality=60)

        with open("albums/"+filename+'.md', 'r+') as file: 
            file_data = file.read() 
            file.seek(0, 0) 
            file.write('---' + '\n') 
            file.write('artist: ' + album['artists'][0]['name'] + '\n' ) 
            file.write('album: ' + album['name'] + '\n') 
            file.write('release_date: ' + album['release_date'] + '\n')
            file.write('cover: ' + album['images'][0]['url'] + '\n')
            file.write('dot: music'+ '\n')
            file.write('layout: post.hbs'+ '\n')
            file.write('---' + '\n' + file_data) 

    total_albums = results['total']
    offset += 51
