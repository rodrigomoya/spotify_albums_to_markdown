# spotipy to md

Python script that allows downloading your list of albums saved in spotify and:

- Download the album covers and generate dithering image per album
- Generate a basic markdown file with YML annotations per album

You need to create an app at https://developer.spotify.com/dashboard/login and get a **client_id** and **client_secret**

```
export SPOTIPY_CLIENT_ID='client_id'
export SPOTIPY_CLIENT_SECRET='client_secret'
export SPOTIPY_REDIRECT_URI='http://localhost:8888/callback'

pip3 install -r requirements.txt
python3 spotify_to_md.py   
```